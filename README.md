# Desafio Labi9 Mobile - Dart e Flutter


## Criar um aplicativo com autenticação e consumo de API

Desafio técnico criado para medir seus conhecimentos e habilidades de programação mobile com Dart e Flutter, utilize a versão estável mais atualizada para desenvolver o desafio.

Não iremos limitar as suas escolhas por ferramentas e bibliotecas, caso faça uso de alguma ferramenta de build, não se esqueça de descrever as instruções para rodarmos o projeto.

Para as requisições utilizar a seguinte [API](https://challenge-labi9-4b4c472d5c07.herokuapp.com/api/docs)

## Fluxo

- Autenticação - O usuário poderá criar uma conta ou fazer login

- Página inicial (Listagem de categorias) - Deve conter as categorias criadas pelo usuário e um botão que levará para a página de criação de categoria.

- Visualização de categoria - Ao clicar em uma categoria, deverá ser mostrado uma listagem com os produtos criados pelo usuário e com a categoria selecionada, junto com um botão para criação de um novo produto.

- Visualização de produto - Ao clicar em um produto, devem ser mostradas as informações sobre ele e um botão que levará para a página para edição de suas informações, assim como a possibilidade de deletar aquele produto.

## O que será avaliado:

- Qualidade e simplicidade do código;

- Estrutura do projeto;

- Padrão de Projeto e boas práticas de Orientação a Objetos;

- Estilização e funcionalidade das páginas

- Componentização de Widgets.

## Diferencial

- Clean Architecture;

- Tratamento de erros;

- Testes cobrindo todos os pontos;

- Utilização do inglês no código;

- Trabalhar offline (persistência dos dados).

## Aplicação

A aplicação tem como objetivo consumir a API fornecida, com as funções de:

- Cadastro de usuário;
- Login de usuário;
- Cadastrar categoria;
- Listar categorias;
- Cadastrar produto;
- Visualizar produto;
- Listar produtos;
- Alterar produto;
- Deletar produto;
- Deslogar da aplicação.

## Entrega

A aplicação deve ser criada em modo privado no seu repositório github ou gitlab, ao termino será necessário enviar um email para comercial@labi9.com e adicionar @GreatGui (caso utilizar github adicionar [GreatGui](https://github.com/GreatGui)) ao repositório.
